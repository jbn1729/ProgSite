import requests, time
sleep = time.sleep
site = 'https://premiers.passoire.fr/'
link_prog = "https://gitlab.com/api/v4/projects/43224931/repository/files/prog_qui_change.py/raw?ref=main"
link = site+'test_syracuse'
prog = ''
pas = 0
nom_fichier = input('Le nom du fichier où se trouve le programme du site au lien '+link_prog+' : ')

try:
    req = requests.get(link_prog)
    ConnectionProg = req.ok
except:
    ConnectionProg = False
if not ConnectionProg:
    print("on ne peut pas accéder au programme qui change sur GitLab, c'est pas grave on continue")

printer = False
while (1):
    try:
        req = requests.get(site)
        acces = req.ok
    except:
        acces=False
        if not printer:
            printer = True
            print("pas d'accès au site")
    if acces:break
    sleep(20)
print('accès au site !')

def modif_prog():
    f = open(nom_fichier)# je le lit juste pas d'inquiétude !
    list_prog = f.readlines()# pour lire toutes les lignes du fichier
    f.close()# très iomportant, à ne pas oublier
    return compile(''.join(list_prog), "<stdin>", "exec")#pour avoir toutes les lignes toutes ensembles


prog = modif_prog()
print('Exécution du nouveau programme !')
exec(prog)
print("fin ; mesure de la largeur de l'interval")
debut = time.process_time()
t = 0
while time.process_time()-debut < 20:# 20 est arbitraire
    l = max_alti(t, t+len_forms)
    t += len_forms
pas = t
print("le pas utilisé pour vous est :", pas)

def recupe():
    global prog
    req = requests.get(link_prog)
    new_prog = req.text
    new_prog2 = new_prog.replace('&#'+'39;', "'")\
                       .replace('&#'+'34;', '"')\
                       .replace('&g'+'t;', '>')\
                       .replace('&l'+'t;', '<')\
                       .replace('&a'+'mp;', '&')
    if compile(new_prog2, '<stdin>', 'exec') != prog:
        print('Un nouveau programme à aller récupérer sur le site et à enregistrer dans le nom du fichier écrit auparavant')

while(1):
    acces = False
    printer = False
    while (1):
        try:
            alti, n_max = main(alti, n_max)# fonction dans le programme
            acces = True
        except:
            acces = False
            if not printer:
                printer = True
                print("plus d'accès au site")
        if acces:break
        sleep(20)
    if printer:
        print("c'est bon, il y a de nouveau un accès")
    try:
        recupe()
    except:
        continue
    n_prog = modif_prog()
    if prog != n_prog:
        prog = n_prog
        print('Exécution du nouveau programme !')
        exec(prog)
        print("fin ; mesure de la largeur de l'interval")
        debut = time.process_time()
        t = 0
        while time.process_time()-debut < 20:# 20 est arbitraire
            l = max_alti(t, t+len_forms)
            t += len_forms
        pas = t
